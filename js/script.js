var CalculatorViewModel = function(dailyOperationTime, secondsPerLift, ampsPerLift, tcAmps) {
    this.dailyOperationTime = ko.observable(dailyOperationTime);
    this.secondsPerLift = ko.observable(secondsPerLift);
    this.ampsPerLift = ko.observable(ampsPerLift);
    this.tcAmps = ko.observable(tcAmps).extend({ numeric: 1 });
    this.efficiency = ko.observable(0.6).extend({'percentage' : true});

    this.numberOfLiftsAt30 = ko.computed(function() {
      var numberOfLiftsAt30 = this.dailyOperationTime() * 0.30 * this.tcAmps() * this.efficiency() / (this.ampsPerLift() * this.secondsPerLift() / 3600);
      return numberOfLiftsAt30.toFixed(2);
    }, this);

    this.numberOfLiftsAt50 = ko.computed(function() {
      var numberOfLiftsAt50 = this.dailyOperationTime() * 0.50 * this.tcAmps() * this.efficiency() / (this.ampsPerLift() * this.secondsPerLift() / 3600);
      return numberOfLiftsAt50.toFixed(2);
    }, this);

    this.maxLifts = ko.computed(function() {
      var numberOfLiftsAt100 = this.dailyOperationTime() * 1.00 * this.tcAmps() * this.efficiency() / (this.ampsPerLift() * this.secondsPerLift() / 3600);
      var maxLifts = numberOfLiftsAt100 + parseFloat(this.numberOfLiftsAt50());
      return maxLifts;
    }, this);

    this.results = ko.observableArray([
      new CalculatorResult(10, this),
      new CalculatorResult(20, this),
      new CalculatorResult(30, this),
      new CalculatorResult(40, this),
      new CalculatorResult(50, this),
      new CalculatorResult(60, this),
      new CalculatorResult(70, this),
      new CalculatorResult(80, this),
      new CalculatorResult(90, this),
      new CalculatorResult(100, this)
    ]);

}


// Class to represent a row in the calculation table
function CalculatorResult(engineDailyOperationPercent, parent) {
    var self = this;
    this.engineDailyOperationPercent = ko.observable(engineDailyOperationPercent).extend({'percentage' : true});

    this.hoursEngineIsRunning = ko.computed(function () {
      var hoursEngineIsRunning = parent.dailyOperationTime() * engineDailyOperationPercent / 100;
      return hoursEngineIsRunning.toFixed(1);
    }, this);
    
    this.maxAmps = ko.computed(function() {
      var maxAmps = parent.tcAmps() * this.hoursEngineIsRunning();
      return maxAmps;
    }, this);

    this.actualAmpsToBatteries = ko.computed(function() {
      var actualAmpsToBatteries = this.maxAmps() * parent.efficiency();
      return actualAmpsToBatteries.toFixed(2);
    }, this);

    this.ampHoursPerLift = ko.computed(function() {
      var ampHoursPerLift = parent.ampsPerLift() * parent.secondsPerLift() / 3600;
      return ampHoursPerLift.toFixed(4);
    }, this);

    this.numberOfLiftsTcOnly = ko.computed(function() {
      var numberOfLiftsTcOnly = this.actualAmpsToBatteries() / this.ampHoursPerLift();
      return numberOfLiftsTcOnly.toFixed(2);
    }, this);

    this.numberOfLifts30Extra = ko.computed(function() {
      var numberOfLifts30Extra = parseFloat(this.numberOfLiftsTcOnly()) + parseFloat(parent.numberOfLiftsAt30());
      return numberOfLifts30Extra.toFixed(2);
    }, this);

    this.numberOfLifts50Extra = ko.computed(function() {
      var numberOfLifts50Extra = parseFloat(this.numberOfLiftsTcOnly()) + parseFloat(parent.numberOfLiftsAt50());
      return numberOfLifts50Extra.toFixed(2);
    }, this);

    // Calculations for Progress Bars

    this.percentageTcOnly = ko.computed(function() {
      var percentageTcOnly = this.numberOfLiftsTcOnly() / parent.maxLifts() * 100;
      return percentageTcOnly;
    }, this);

    this.percentage30Extra = ko.computed(function() {
      var percentage30Extra = this.numberOfLifts30Extra() / parent.maxLifts() * 100;
      percentage30Extra = percentage30Extra - this.percentageTcOnly();
      return percentage30Extra;
    }, this);


    this.percentage50Extra = ko.computed(function() {
      var percentage50Extra = this.numberOfLifts50Extra() / parent.maxLifts() * 100;
      percentage50Extra = percentage50Extra - this.percentage30Extra() - this.percentageTcOnly();
      return percentage50Extra;
    }, this);


}

ko.extenders.percentage = function(target, option) {
    target.percentage = ko.computed({
        read: function() {
            return this() * 100;
        },
        write: function(value) {
            this(value/100);
        },
        owner: target
    });
    return target;
};

ko.applyBindings(new CalculatorViewModel("8", "20", "200", "20"));

// Sliders
$('#daily-op-time').slider({
  //tooltip: 'always',
  formater: function(value) {
    return value + ' Hours';
  }
});

$('#daily-op-timer').slider({
  //tooltip: 'always',
  formater: function(value) {
    return value + ' Hours';
  }
});

$('#seconds-per-lift').slider({
  //tooltip: 'always',
  formater: function(value) {
    return value + ' Seconds';
  }
});

$('#amps-per-lift').slider({
  //tooltip: 'always',
  formater: function(value) {
    return value + ' Amps';
  }
});

$('#test-slider').slider({
  //tooltip: 'always',
  formater: function(value) {
    return value + ' Test';
  }
});

// Toggleswitch
$("#tc-amps").bootstrapSwitch({
  onText: "50",
  offText: "20",
  labelText: "Amps",
  onSwitchChange: function(event, state) {
    var newAmps = $('#tc-amps').val() == 50 ? 20 : 50
    $('#tc-amps').val(newAmps).change();
  }
});

// Align the progress bars with the table
var tableHeadHeight = $('.results-table-container thead').height();
$('.results-visual-container').css('margin-top', tableHeadHeight - 57);

$(window).resize(function(){
  var tableHeadHeight = $('.results-table-container thead').height();
  $('.results-visual-container').css('margin-top', tableHeadHeight - 57);
});


